﻿using System;
using System.Text;

namespace SecondLab
{
    public class Pair
        {
            public static int Сount = 0;
    
            protected long Numerator { get; set; }
    
            protected UInt16 Denominator { get; set; }
    
            protected Pair()
            {
                Сount++;
                Numerator = 0;
                Denominator = 1;
            }
    
            protected Pair(long numerator)
            {
                Сount++;
                Numerator = numerator;
                Denominator = 1;
            }
    
            protected Pair(long numerator, ushort denominator)
            {
                Сount++;
                Numerator = numerator;
                Denominator = denominator;
            }
    
            public void Input()
            {
                Console.Write("Введите числитель = ");
                Numerator = Convert.ToInt64(Console.ReadLine());
                Console.Write("Введите знаменатель = ");
                Denominator = Convert.ToUInt16(Console.ReadLine());
            }
    
            protected bool Compare(Pair anotherTriad)
            {
                return Numerator == anotherTriad.Numerator && Denominator == anotherTriad.Denominator;
            }
    
            public override String ToString()
            {
                StringBuilder stringBuilder = new StringBuilder();
    
                stringBuilder.Append(Numerator.ToString());
                stringBuilder.Append("/");
                stringBuilder.Append(Denominator.ToString());
    
                return stringBuilder.ToString();
            }
    
            protected void Display()
            {
                Console.WriteLine("Пара чисел " + ToString());
            }
        }
    
        class Fraction : Pair
        {
            public static Fraction operator +(Fraction firstFraction, Fraction secondFraction)
            {
                return new Fraction
                {
                    Numerator = firstFraction.Numerator * secondFraction.Denominator + secondFraction.Numerator * firstFraction.Denominator,
                    Denominator = (ushort) (firstFraction.Denominator * secondFraction.Denominator)
                };
            }
    
            public static Fraction operator -(Fraction firstFraction, Fraction secondFraction)
            {
                return new Fraction
                {
                    Numerator = firstFraction.Numerator * secondFraction.Denominator - secondFraction.Numerator * firstFraction.Denominator,
                    Denominator = (ushort) (firstFraction.Denominator * secondFraction.Denominator)
                };
            }
    
            public static Fraction operator *(Fraction firstFraction, Fraction secondFraction)
            {
                return new Fraction()
                {
                    Numerator = firstFraction.Numerator * secondFraction.Numerator,
                    Denominator = (ushort) (firstFraction.Denominator * secondFraction.Denominator)
                };
            }
    
            public static bool operator ==(Fraction firstFraction, Fraction secondFraction)
            {
                Fraction tempNumber = firstFraction - secondFraction;
                return tempNumber.Numerator == 0;
            }
    
            public static bool operator !=(Fraction firstFraction, Fraction secondFraction)
            {
                Fraction tempNumber = firstFraction - secondFraction;
                return tempNumber.Numerator != 0;
            }
    
            public static bool operator >(Fraction firstFraction, Fraction secondFraction)
            {
                Fraction tempNumber = firstFraction - secondFraction;
                return tempNumber.Numerator > 0;
            }
    
            public static bool operator <(Fraction firstFraction, Fraction secondFraction)
            {
                Fraction tempNumber = firstFraction - secondFraction;
                return tempNumber.Numerator < 0;
            }
            
            public override bool Equals(object other)
            {
                if (other.GetType() == GetType())
                {
                    return this == (Fraction) other;
                }

                return false;
            }

            public override int GetHashCode()
            {
                // Не используется сравнение по хешу, нечего хешировать
                return 0;
            }
        }
        
        class Program
        {
            static void Main(string[] args)
            {
                Fraction firstNumber = new Fraction();
                firstNumber.Input();
    
                Fraction secondNumber = new Fraction();
                secondNumber.Input();
    
                Console.WriteLine($"Сумма дробей {firstNumber} + {secondNumber} = {(firstNumber + secondNumber)}");
                Console.WriteLine($"Разность дробей {firstNumber} - {secondNumber} = {firstNumber - secondNumber}");
                
                Console.WriteLine($"Произведение дробей {firstNumber} * {secondNumber} = {firstNumber * secondNumber}");
                
                Console.WriteLine($"Сравнение дробей {firstNumber} == {secondNumber} = {firstNumber == secondNumber}");
                Console.WriteLine($"Сравнение дробей {firstNumber} != {secondNumber} = {firstNumber != secondNumber}");
                Console.WriteLine($"Сравнение дробей {firstNumber} > {secondNumber} = {firstNumber > secondNumber}");
                Console.WriteLine($"Сравнение дробей {firstNumber} < {secondNumber} = {firstNumber < secondNumber}");
                
                Console.WriteLine("Нажмите Enter для выхода...");
                Console.ReadLine();
            }
        }
}